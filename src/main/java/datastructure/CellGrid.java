package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows;
    private int cols;
    private CellState[][] grid; //Skal implementere et to dimensjonalt array som vårt grid
    //Datatype [] or multiple [] create an array of that datatype
    private CellState intiaState;


    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows=rows;
        this.cols=columns; //Setter feltvariabelen lik input paramteren
        this.intiaState=initialState;

        grid=new CellState[this.rows][this.cols]; //Lager et to dimensjonalt array her
        for (int row=0;row<rows;row++){
            for (int col=0;col<cols;col++){ //Her lager vi først en for loop som går i gjennom hver rad verdi, så den begynner med rad 1
                //Og så kommer vi ned i neste for loop som begynner i kolonne 1, og så går videre til kolonne 2 men framdeles rad 1, slik gjør den til den har gått gjennom alle kolonner for første rad før den går videre
                grid[row][col]=initialState; //Gjør det samme som vi gjorde i ball klassen, setter hver celle i gridet lik initialstate
                //Vi trenger en defualt verdi til å legge i hver celle(rad,kolonne)

            }
        }

	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if (row<0 || column<0){
            throw new IndexOutOfBoundsException(); //Dersom vi angir en negativ verdi for rad og kolonne verdi vi ønnsker å sette inn noe så vil vi få ut en exception!
        }
        if (row>numRows() || column>numColumns()){
            throw new IndexOutOfBoundsException(); //Dersom vi angir en verdi i metoden som er større enn antall rader eller kolonner som vi har angitt når vi instanierte objektet med konstruktøren så skal vi få en exception
            //Ettersom vi ikke kan gå utenfor antallet kolonner og rader vi har!
        }
        grid[row][column]=element;



        

        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if(row<0 || column<0){
            throw new IndexOutOfBoundsException();
        }
        if (row>numRows() || column>numColumns()){
            throw new IndexOutOfBoundsException(); 
        }
        //Samme her vi kan ikke finne verdien i en celleverdi som er negativ eller som er større enn hva vi anga i konstruktøren.
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid newGrid=new CellGrid(this.rows, this.cols, this.intiaState);
        for (int row=0;row<rows;row++){
            for (int col=0;col<cols;col++){
                newGrid.set(row,col,this.grid[row][col]);//Siden den er av typen cellgrid så har vi de metodene vi har laget tilgjengelig til å bruke
            }
        }//Denne metoden sørger for at vi holder oss til det antallet og kolonner som vi laget i vårt forrige grid
        //Deretter bruker vi set metoden til å sette inn verdier i hver kolonne, denne verdien skal være lik hver verdi vi har i samme rad og kolonne fra vårt forrige grid
        return newGrid;
        
        //Siden retur typen skal være av typen IGrid så kan vi ikke instatiere et nytt objekt fordi cellstate er ikke en under klasse av IGrid
    }
    
}
