package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		// TODO
		return currentGeneration.numRows(); //Currentgeneration er av typen cellgrid og kan derfor bruke metodene vi implementerte for å hente ut number of rows and number of columns
	}

	@Override
	public int numberOfColumns() {
		// TODO
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		// TODO
		for (int row=0;row<currentGeneration.numRows();row++){
			for (int col=0;col<currentGeneration.numColumns();col++){
				currentGeneration.set(row, col, getNextCell(row, col));
			}
		}
		this.currentGeneration=nextGeneration;
		

	}

	@Override
	public CellState getNextCell(int row, int col) {
		// TODO
		CellState cell=getCellState(row, col);
		int numAliveNeighbours=countNeighbors(row, col, CellState.ALIVE); //Vi ønsker å telle antall levende naboer
		if (cell==CellState.ALIVE){
			if (numAliveNeighbours<2 || numAliveNeighbours>3){
				return CellState.DEAD;
			}
			else{
				return CellState.ALIVE;
			}
		
		}
		if (cell==CellState.DEAD){
			if(numAliveNeighbours==3){
				return CellState.ALIVE;
			}
		}
		return CellState.DEAD;
		
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		//Feilen ligger i count metoden ikke i rules metoden!!
		// TODO
		//Countneighbours metoden er en hjelpemetode for getnextcell metoden og skal hjelpe oss med å implementere spillreglene.
		if (row>numberOfRows() || col>numberOfColumns()){
			throw new IndexOutOfBoundsException();
		} //Sørger for at det ikke er mulig å sende inn verdier som ikke er i griddet
		//I denne metoden skal vi hente ut cellstate til alle naboer også telle opp hvor mange som er alive og dead
		//Og denne metoden skal benyttes getNextcell metode
		//Så i denne metoden får vi som input en rad og en kolonne verdi og i tillegg en state verdi vi skal telle for
		//Så når vi står i en celle så skal vi teller hvor mange naboer som har verdien til variabelen state;
		//Derfor trenger vi en variabel counter som sjekker antallet tilfeller 
		int counter = 0;
		for (int rows=row-1;rows<=row+1;rows++){
			for (int cols=col-1;cols<=col+1;cols++){
				if(  (rows<0 || cols<0) || (rows==row && cols==col) ){ //Må sørge for at vi ikke går utenfor griddet
					//Vi må har mindre enn 0 fordi vi har et array som alltid vil starte på posisjonen lik null, så eneste måten å være utenfor er ved å være negativ
					continue;
				}
				if (getCellState(rows, cols).equals(state)){
					counter++;
				}
			}
		}
		return counter;
	}
	
	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
